<?php
header('Content-Type: text/html; charset=UTF-8');
include 'db_info.php';
if(session_start() && $_SESSION['token']!=$_POST['token_chg']) {
    exit();
}
    $id = $db->quote($_POST['chg_by_id']);
    $request = "SELECT * FROM form7 where id=$id";
    $sth = $db->prepare($request);
    $sth->execute();
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    if ($data == false) {
        header('Location:admin.php');
        exit();
    }
    $_SESSION['login'] = $data['login'];
// Записываем ID пользователя.
    $_SESSION['uid'] = $id;

    setcookie('admin', '1');
// Делаем перенаправление.
    header('Location: index.php');
