<?php
/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="For lab6"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
include 'db_info.php';

$request = "SELECT * from admin";
$result = $db->prepare($request);
$result->execute();
$flag=0;
while($data=$result->fetch()){
    if($data['login']==strip_tags($_SERVER['PHP_AUTH_USER']) && password_verify($_SERVER['PHP_AUTH_PW'],$data['pass'])){
        $flag=1;
    }
}
if($flag==0){
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="For lab6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
print('Вы успешно авторизовались и видите защищенные паролем данные.');
print '<div>Привет админ!</div>';
print '<br/>';
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
session_start();
$_SESSION['token'] = uniqid();
$token = $_SESSION['token'];
?>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Админка для 7 лабы</title>
    <link rel="stylesheet" media="all" href="style.css"/>

</head>
<body>
<form action="delete.php" method="post" accept-charset="UTF-8">
    <label>
        <input type="hidden" name="token_del" <?php print "value = '$token'";?>>
        <input type="number" name="del_by_id">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Удалить запись по ID">
</form>
<form action="change.php" method="post" accept-charset="UTF-8">
    <label>
        <input type = "hidden" name="token_chg" <?php print "value = '$token'";?>>
        <input type="number" name="chg_by_id">
    </label>
    <input type="submit" style="margin-bottom : -1em"  class="buttonform" value="Изменить запись по ID">
</form>
<?php
    function print_ability($abil){
        if($abil=='imm')print 'Бессмертие';
        elseif($abil=='ph')print 'Прохождение сквозь стены';
        elseif($abil =='lv')print 'Левитация';
        else print 'Error';
    }
    $request = "SELECT * from form7  order by id";
    $result = $db ->prepare($request);
    $result->execute();
    $request = "SELECT * FROM abil7 order by id";
    $result_abil = $db->prepare($request);
    $result_abil->execute();
    $data_abil = $result_abil->fetch();
    print '<table class="table">';
    print '<tr><th>ID</th><th>Имя</th><th>E-Mail</th><th>Дата рождения</th><th>Пол</th><th>Кол-во конечностей</th>
    <th>Биография</th><th>Логин</th><th>Хэш пароля</th><th>Способность</th></tr>';
    while($data = $result->fetch()){
        print '<tr><td>';
        print $data['id'];
        print '</td><td>';
        print strip_tags($data['name']);
        print '</td><td>';
        print strip_tags($data['email']);
        print '</td><td>';
        print strip_tags($data['year']);
        print '</td><td>';
        print strip_tags($data['sex']);
        print '</td><td>';
        print intval($data['lb']);
        print '</td><td>';
        print strip_tags($data['bio']);
        print '</td><td>';
        print strip_tags($data['login']);
        print '</td><td>';
        print strip_tags($data['pass']);
        print '</td><td>';
        if($data_abil['id']==$data['id']){
            do{
                print strip_tags(print_ability($data_abil['ability']));
                print '<br/>';
                $data_abil = $result_abil->fetch();
            }while($data_abil['id']==$data['id']&& $data_abil);
        }
        print '</td></tr>';
    }
    print '</table>';

    $request = "SELECT COUNT(ability) FROM abil7 where ability='imm' group by ability";
    $result = $db ->prepare($request);
    $result->execute();
    $data_im = $result->fetch()[0];
    $request = "SELECT COUNT(ability) FROM abil7 where ability='ph'";
    $result = $db ->prepare($request);
    $result->execute();
    $data_ph = $result->fetch()[0];
    $request = "SELECT COUNT(ability) FROM abil7 where ability='lv' group by ability";
    $result = $db ->prepare($request);
    $result->execute();
    $data_lv = $result->fetch()[0];
    print '<h2>Статистика по сверхспособностям:</h2>';
    print '<table class="table">';
    print '<tr><th>Бессмертие</th><th>Прохождение сквозь стены</th><th>Левитация</th></tr>';
    print '<tr><td>';
    print $data_im;
    print '</td><td>';
    print $data_ph;
    print '</td><td>';
    print $data_lv;
    print '</td></tr>';
    print '</table>';

?>


</body>